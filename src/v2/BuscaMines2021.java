package v2;

import java.util.Random;
import java.util.Scanner;

/**
 * 
 * @author IES SABADELL
 * M03UF2. Programa que permet jugar al Buscamines, especificant les dimensions del tauler i el número de mines
 */
public class BuscaMines2021 {
	public static Scanner teclat = new Scanner(System.in);
	public static int FilaTope;
	public static int ColumnaTope;
	public static final int TOPE = 12;
	public static final int MIN = 3;
	public static final int BUIT = 0;
	public static final int MINA = 1;
	public static final int TAPAT = 9;
	public static final int MARCADA = -1;
	public static final int BOMBAZO = -2;
	
	public static Random r = new Random();
	public static boolean partidaEnCurs;
	public static int jugafila;
	public static int jugacolumna;

	public static void main(String[] args) {
		
		int[][] mines = null;	//secret
		int[][] camp = null; 	//tauler d'usuari
		int opcio;
		
		System.out.println("Buscamines");
		System.out.println();
		do {
			opcio = menuPrincipal();
			switch (opcio) {
			case 0:
			default:
				break;
			case 1:
				ajuda();
				break;
			case 2:
				opcions();
				camp = new int[Partida.FilesTope][Partida.ColumnesTope];
				mines = new int[Partida.FilesTope][Partida.ColumnesTope];						
				inicialitzar_mines(mines);
				inicialitzar_camp(camp);
				break;
			case 3:
				inicialitzar_mines(mines);
				inicialitzar_camp(camp);
				if (Partida.configurada) {
					partidaEnCurs = true;
					do {
						veure_tauler(camp);
						//System.out.println("SECRET");
						//veure_tauler(mines);
						partidaEnCurs = jugar(camp, mines);
					} while (partidaEnCurs && quedenCaselles(camp));
					veure_tauler(camp);
					if (partidaEnCurs) {
						Partida.guanyades++;
						System.err.println("Partida guanyada");
					}
					else {
						Partida.perdudes++;
						System.err.println("Partida perduda");
					}
				}
				break;
			case 4:
				mostrarEstadistiques();
				break;
			}
		} while (opcio != 0);
	}

	public static void mostrarEstadistiques() {
		System.out.println("\n\n\n****************************************");
		System.out.println("Partides Guanyades: " + Partida.guanyades);
		System.out.println("Partides  Perdudes: " + Partida.perdudes);
		System.out.println("****************************************\n\n\n");
	}
	
	public static boolean quedenCaselles(int[][]tauler) {
		boolean lliures = false;
		for (int i= 0;i<Partida.FilesTope;i++)
			for (int j=0;j<Partida.ColumnesTope;j++)
				if(tauler[i][j]==TAPAT) {
					lliures = true;
					break;
				}
		return lliures;
	}
	
	public static int obtenirFila() {
		int f;
		do {
			System.out.println("Digues una fila (0.." + (Partida.FilesTope-1 + ")")); 
			f = teclat.nextInt();
		}while(f<0 || f>Partida.FilesTope-1);
		return f;
	}

	public static int obtenirColumna() {
		int c; 
		do {	
			System.out.println("Digues una columna (0.." + (Partida.ColumnesTope-1 + ")")); 
			c=teclat.nextInt();
		}while (c<0 || c>Partida.ColumnesTope-1);
		return c;
	}

	public static boolean jugar(int camp[][], int mines[][]) {
		boolean resultat = true;
		String queFer;

		/*
		 f = obtenir fila correcta
		 c = obtenir columna correcta
		 comprovar si la casella camp[jugafila][jugacolumna] està destapada o no
		 if (camp[f][c] == TAPAT)
				preguntar si queremos destapar o marcar mina
				Si marca mina 
				 	camp[f][c] = MARCADA
				Else					
					Si casella conte mina (mines[f][c]==MINA)
						<<perd>>
						retornar false
					Else
						"expandir" 
					fisi
				fisi
		fisi
		// falta poder desmarcar una casella
*/
		
		int jugafila = obtenirFila();
		int jugacolumna = obtenirColumna();
		
		if (camp[jugafila][jugacolumna]==TAPAT) {
			boolean ok = false;
			do {
				System.out.println("Vols (D)destapar o (M)marcar mina");
				queFer = teclat.next();
				switch (queFer.charAt(0)) {
				case 'd':
				case 'D': if (mines[jugafila][jugacolumna]==MINA) {
							System.out.println("Havia mina!!!");
							camp[jugafila][jugacolumna]=BOMBAZO;
							System.err.println("Booom");
							resultat = false;
						  }
						  else {//expansió !!!
							  gestionar(camp,mines,jugafila,jugacolumna);							  
						  }
						  ok = true;
						  break;
				case 'm': 
				case 'M': camp[jugafila][jugacolumna] = MARCADA;
						  ok = true;
						  break;
				default:  System.out.println("Si us plau, D/M");
				}
			} while (!ok);
		}
		else
			System.err.println("Casella ja destapada");
		return resultat;
	}

	public static void gestionar (int camp[][], int mines[][], int f, int c) {
		int bombes = 0;
		if (camp[f][c] != TAPAT)	return; 
		
		//mirar fila superior
		if (f>0 && c>0) 
			if (mines[f-1][c-1]==MINA) bombes ++;  //(f-1,c-1)
		if (f>0) 
			if (mines[f-1][c]==MINA) bombes ++;  //(f-1,c)
		if (f>0 && c<Partida.ColumnesTope-1) 
			if (mines[f-1][c+1]==MINA) bombes ++;  //(f-1,c+1)
		
		//mirar fila actual
		if(c>0)
			if (mines[f][c-1]==MINA)	bombes ++; // (f,c-1)
		if(c<Partida.ColumnesTope-1)
			if (mines[f][c+1]==MINA)	bombes ++; // (f,c+1)
		
		//mirar fila posterior
		if (f<Partida.FilesTope-1 && c>0) 
			if (mines[f+1][c-1]==MINA) bombes ++;  //(f+1,c-1)
		if (f<Partida.FilesTope-1) 
			if (mines[f+1][c]==MINA) bombes ++;  //(f+1,c)
		if (f<Partida.FilesTope-1 && c<Partida.ColumnesTope-1) 
			if (mines[f+1][c+1]==MINA) bombes ++;  //(f+1,c+1)

		camp[f][c] = bombes;
		if (bombes ==0) {
			if (f>0 && c >0)		gestionar(camp,mines,f-1,c-1);
			if (f>0)				gestionar(camp,mines,f-1,c);
			if (f>0 && c< Partida.ColumnesTope-1)	gestionar(camp,mines,f-1,c+1);
			if (c>0)							gestionar(camp,mines,f,c-1);
			if (c< Partida.ColumnesTope-1)		gestionar(camp,mines,f,c+1);
			if(	f<Partida.FilesTope-1 && c>0)		gestionar(camp,mines,f+1,c-1);
			if (f<Partida.FilesTope-1)			gestionar(camp,mines,f+1,c);
			if (f<Partida.FilesTope-1 && c<Partida.ColumnesTope-1)	gestionar(camp,mines,f+1,c+1);
		}
	}

	public static void opcions() {
		do {
			System.out.println("Digues les columnes del tauler:");
			Partida.ColumnesTope = teclat.nextInt();
		} while (Partida.ColumnesTope > TOPE || Partida.ColumnesTope < MIN);
		
		do {
			System.out.println("Digues les files del tauler");
			Partida.FilesTope = teclat.nextInt();
		} while (Partida.FilesTope > TOPE || Partida.FilesTope < MIN);
		
		do {
			System.out.println("Digues les mines que hi ha al tauler:");
			Partida.mines = teclat.nextInt();
		} while (Partida.mines > (Partida.ColumnesTope * Partida.FilesTope) / 3);
		
		Partida.configurada = true;
	}

	public static void inicialitzar_mines(int[][] mines) {
		int x, y;
		for (int f = 0; f < Partida.FilesTope; f++)
			for (int c = 0; c < Partida.ColumnesTope; c++)
				mines[f][c] = BUIT;

		for (int i = 0; i < Partida.mines; i++) {
			x = r.nextInt(Partida.FilesTope);
			y = r.nextInt(Partida.ColumnesTope);
			if (mines[x][y] == BUIT) {
				mines[x][y] = MINA;
			} else
				i--;
		}
	}

	public static void inicialitzar_camp(int[][] camp) {
		for (int f = 0; f < Partida.FilesTope; f++)
			for (int c = 0; c < Partida.ColumnesTope; c++)
				camp[f][c] = TAPAT;
	}

	public static void veure_tauler(int[][] tauler) {

		for (int f = 0; f < Partida.FilesTope; f++) {
			System.out.print(f + " ║");
			for (int c = 0; c < Partida.ColumnesTope; c++) {
				if (tauler[f][c] == 9)
					System.out.print("  ·");
				else if (tauler[f][c] == 0)
					System.out.print("   ");
				else if (tauler[f][c] < 0)
					System.out.print(" " + tauler[f][c]);
				else 
					System.out.print("  " + tauler[f][c]);
			}
			System.out.println();
		}
		System.out.print("  ╚");
		for (int f = 0; f < Partida.ColumnesTope; f++) {
			System.out.print("═══");
		}
		System.out.println("═");
		System.out.print("   ");
		for (int f = 0; f < Partida.ColumnesTope; f++) {
			System.out.print("  " + f);
		}
		System.out.println();
	}

	public static void ajuda() {
		System.out.println("El juego consiste en despejar todas las casillas de una pantalla que no oculten una mina.\n"
				+ "Algunas casillas tienen un número, el cual indica la cantidad de minas que hay en las casillas circundantes.\n"
				+ "Así, si una casilla tiene el número 3, significa que de las ocho casillas que hay alrededor (si no es en una esquina o borde) \n"
				+ "hay 3 con minas y 5 sin minas. Si se descubre una casilla sin número indica que ninguna de las casillas vecinas tiene mina y éstas \n"
				+ "se descubren automáticamente.\r\n"
				+ "Si se descubre una casilla con una mina se pierde la partida.");
		System.out.println();
	}

	public static int menuPrincipal() {
		int opcio;
		System.out.println("1.- Ajuda");
		System.out.println("2.- Inicialitzar Joc");
		System.out.println("3.- Jugar Partida");
		System.out.println("4.- Ranking (WiP)");
		System.out.println("0.- Sortir");
		do {
			System.out.println("Escull una opció:");
			opcio = teclat.nextInt();
			if (opcio > 5 || opcio < 0)
				System.err.println("Aquesta opció no es correcta");
		} while (opcio > 5 || opcio < 0);
		return opcio;
	}
}
